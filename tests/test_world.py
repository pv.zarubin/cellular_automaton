from typing import List

import pytest
import numpy

import ca.world


@pytest.mark.parametrize(
    'world_state, expected_state',
    [
        (
               [1],
            [0, 1, 0]
        ),
        (
               [0, 1, 0],
            [0, 1, 1, 0, 0]
        ),
        (
               [0, 1, 1, 0, 0],
            [0, 1, 1, 1, 0, 0, 0]
        ),
        (
               [0, 1, 1, 1, 0, 0, 0],
            [0, 1, 1, 0, 1, 0, 0, 0, 0]
        ),
    ]  # noqa
)
def test_world(world_state: List[int], expected_state: List[int]) -> None:
    """Проверяем соблюдение правил 110 правила.

    :param world_state: данное состояние мира
    :param expected_state: ожидаемое следующее состояние мира
    """
    world_state = numpy.array(world_state, dtype=int)

    world = ca.world.World(110, initial_state=world_state)
    state = world.step()

    assert numpy.array_equal(
        state,
        numpy.array(expected_state)
    )
