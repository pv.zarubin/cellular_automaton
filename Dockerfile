FROM python:3.6

WORKDIR /srv/ca
COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY ./ /srv/ca

CMD ["python3", "-m", "ca"]