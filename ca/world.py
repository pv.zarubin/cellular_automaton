from typing import Tuple, NamedTuple, List, Set, Optional

import numpy


class World:
    def __init__(self, rule: int, initial_state: Optional[numpy.array] = None) -> None:
        """Инициализируем мир элементарных клеточных автоматов.

        :param rule: номер правила
        :param initial_state: если не передано мир начинается с одной живой клетки
        """
        self.rule = self.rule_patterns(rule)

        if initial_state is None:
            self.world = numpy.array([0, 1, 0], dtype=int)  # первая живая клетка
        else:
            self.world = numpy.zeros(len(initial_state) + 2, dtype=int)
            numpy.copyto(self.world[1:1 + len(initial_state)], initial_state)

    def rule_patterns(self, rule_number: int, patterns: List[int] = [111, 110, 101, 100, 11, 10, 1, 0]) -> Set[int]:
        """Возвращает set паттернов которые для указанного `rule_number` дают живую клетку.

        Правила элементарных клеточных автоматов выглядят следующим образом:
            - такие автоматы одномерны, отсюда вытекает что соседних клеток у клетки всего две
            - итого 8 паттернов от 111 - все клетки живы, до 000 все клетки мертвы
            - rule_number это беззнаковый байт, каждый бит этого числа означает какой результат дает
              соответствующий паттерн: 1 - жива, 0 - мертва.

        :param rule_number: однобайтовый безнаковый номер правила
        :param patterns: паттерны
        """
        assert 0 < rule_number < 255

        rule = reversed([
            bool(rule_number & (1 << i))
            for i in range(8)
        ])
        return {
            patterns[i]
            for i, truth in enumerate(rule)
            if truth
        }

    def get_pattern(self, x: int, world: numpy.array) -> int:
        """Возвращает паттерн соответствующий переданной координате.

        :param x: координата в мире
        :param world: мир
        """
        pattern: int = 0

        if world[x - 1]:
            pattern += 100
        if world[x]:
            pattern += 10
        if world[x + 1]:
            pattern += 1

        return pattern

    def step(self) -> numpy.array:
        """Посчитать мир один раз."""

        prev_state = self.world
        self.world = numpy.zeros(len(prev_state) + 2, dtype=int)
        visible_world = self.world[1: 1 + len(prev_state)]

        for x in range(1, len(prev_state) - 1):
            if self.get_pattern(x, prev_state) in self.rule:
                visible_world[x] = 1
            else:
                visible_world[x] = 0

        return visible_world
