from typing import Tuple, NamedTuple, List, Set
import time

import pygame
import numpy

import ca.world


def main(rule_number: int, width: int = 1000, height: int = 700) -> None:
    """Запускает элементарный клеточный автомат с указанным правилом.

    :param rule_number: номер правила
    :param width: ширина окна
    :param height: высота окна
    """
    world = ca.world.World(rule_number)

    pygame.init()

    running: bool = True
    pause: bool = False
    smooth: bool = True
    clock: pygame.time.Clock = pygame.time.Clock()
    screen: pygame.Surface = pygame.display.set_mode((width, height))
    epoch: int = 1
    prev_world_screen = None

    while running:
        clock.tick()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    # Пробелом можно ставить игру на паузу
                    pause = not pause
                if event.key == pygame.K_s:
                    # Нажав S можно включать/выключать сглаживание
                    smooth = not smooth

        if not pause:
            pygame.display.set_caption(f'FPS: {clock.get_fps()}')

            world_state = world.step()
            color_state = numpy.array(
                [
                    [0xffffff] if x else [0x0]
                    for x in world_state
                ],
                dtype=int
            )
            world_screen = pygame.Surface((len(world_state), epoch))
            if prev_world_screen:
                world_screen.blit(
                    prev_world_screen,
                    (1, 0, world_screen.get_width() - 2, world_screen.get_height() - 1)
                )

            pygame.surfarray.blit_array(
                world_screen.subsurface((0, world_screen.get_height() - 1, world_screen.get_width(), 1)),
                color_state
            )

            scale = screen.get_height() / screen.get_width()
            if smooth:
                scale_f = pygame.transform.smoothscale
            else:
                scale_f = pygame.transform.scale
            scale_f(
                world_screen.subsurface(
                    (
                        0,
                        0,
                        world_screen.get_width(),
                        world_screen.get_height() * scale
                    )
                ),
                (width, height),
                screen
            )

            pygame.display.flip()

            prev_world_screen = world_screen
            epoch += 1
        else:
            pygame.display.set_caption('PAUSED')
            time.sleep(0.1)


if __name__ == '__main__':
    main(110)
